CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `established` date DEFAULT NULL,
  `employee_count` int(11) DEFAULT NULL,
  `logo` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'STARTUPFORSUPEROMNIVA!','2018-05-17',2,'1f44b95a-f7d6-44ba-8cdb-75165f9bffcc.jpg'),(2,'Proovime','2018-05-15',132132,'d91f26d7-d9f8-4606-b258-27a596c7b3c9.jpg');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Delivery'),(2,'Discovery'),(3,'Test');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_form`
--

DROP TABLE IF EXISTS `feedback_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meeting_uuid` varchar(200) NOT NULL,
  `feedback_points` int(11) DEFAULT NULL,
  `feedback_comments` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_form`
--

LOCK TABLES `feedback_form` WRITE;
/*!40000 ALTER TABLE `feedback_form` DISABLE KEYS */;
INSERT INTO `feedback_form` VALUES (113,'c9392514-d83a-4345-b8ab-e5576315fe58',4,'Test1'),(114,'c9392514-d83a-4345-b8ab-e5576315fe58',3,'Test2'),(115,'c9392514-d83a-4345-b8ab-e5576315fe58',2,'Test3'),(116,'a2544cfe-b7d3-4f27-8925-ec08f4909275',3,'ttt'),(117,'c9392514-d83a-4345-b8ab-e5576315fe58',4,'Proov');
/*!40000 ALTER TABLE `feedback_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meeting`
--

DROP TABLE IF EXISTS `meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting` (
  `uuid` varchar(200) DEFAULT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `details` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `meeting_owner_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting`
--

LOCK TABLES `meeting` WRITE;
/*!40000 ALTER TABLE `meeting` DISABLE KEYS */;
INSERT INTO `meeting` VALUES ('CEA46166D358411A8A6ABFC556456C93','subject','details','12312312','16:30','Ajurünnakud',2,1),('a2544cfe-b7d3-4f27-8925-ec08f4909275','test31','test1','','1:00','null',2,15),('c9392514-d83a-4345-b8ab-e5576315fe58','asdfasdfasf','asdfasdf','2018-05-17','10:00','Ajurünnakud',1,24),('d092bc7f-e1a0-4a54-b806-60a2089b1718','Test','teast','2018-05-14','5:30','Progressi ülevaated',1,31),('f1182c6f-6d46-4733-b906-0f28b9c2b5b6','Testime','test','2018-05-22','5:30','Progressi ülevaated',2,32),('6a85f6e7-f452-496d-b9e9-a047991a0055','TEST13.19','kell on 13.19','2018-05-31','15:30','Ajurünnakud',2,33),('137ed352-4291-4918-8c57-50d20ad0c06b','Testime 13.21','13.21 on kell','2018-05-31','16:30','Koolitused',2,34),('7d685a09-344d-46a6-bd03-04036fb53eab','test13.28','test13.28','2018-05-13','15:00','Tutvustus koosolekud',1,35),('237f2bac-ab3b-40ee-bd4e-8e6449485394','MIngi ajasurnuksloomine','Tark jutt!','2018-05-18','6:15','Läbirääkimised',22,36),('f4c3baeb-87f0-4f4a-97dc-c476df6bd2d7','aaa','aaa','2018-05-14','1:00','Progressi ülevaated',1,56),('328ffe1c-0de8-4032-9688-1dce090d4851','aaa','aaa','2018-05-30','1:00','Teami juhtimiskoosolekud',1,57),('0de52472-f2a6-48e3-9d1b-e96e02aaddd7','Test','test11.40','2018-05-16','11:45','Teami juhtimiskoosolekud',1,59);
/*!40000 ALTER TABLE `meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meeting_type`
--

DROP TABLE IF EXISTS `meeting_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meeting_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `sub_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meeting_type`
--

LOCK TABLES `meeting_type` WRITE;
/*!40000 ALTER TABLE `meeting_type` DISABLE KEYS */;
INSERT INTO `meeting_type` VALUES (1,'Teami juhtimiskoosolekud','INFO KOOSOLEKUD'),(2,'1:1','INFO KOOSOLEKUD'),(3,'Progressi ülevaated','INFO KOOSOLEKUD'),(4,'Tegevuste ülevaatekoosolekud','INFO KOOSOLEKUD'),(5,'Planeerimis koosolekud','INFO KOOSOLEKUD'),(6,'Workshopid/Töötoad','INFO KOOSOLEKUD'),(7,'Ajurünnakud','INFO KOOSOLEKUD'),(8,'Info kogumis koosolekud','INFO KOOSOLEKUD'),(9,'Tutvustus koosolekud','INFO KOOSOLEKUD'),(10,'Broadcasting/Ülekanded','INFO KOOSOLEKUD'),(11,'Koolitused','INFO KOOSOLEKUD'),(12,'Probleemi lahendamis koosolekud','OTSUSTUS KOOSOLEKUD '),(13,'Otsuse koosolekud','OTSUSTUS KOOSOLEKUD '),(14,'Läbirääkimised','OTSUSTUS KOOSOLEKUD '),(15,'Organisatsiooni juhtimiskoosolekud','OTSUSTUS KOOSOLEKUD ');
/*!40000 ALTER TABLE `meeting_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `role_id` varchar(45) NOT NULL,
  `department` varchar(45) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `user_uuid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_uuid_UNIQUE` (`user_uuid`),
  KEY `fk_user_user_roles1_idx` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Anton','Sokolov','pass','admin','ValiIT','sokolovmeister@gmail.com','57b689bc-df9f-4d46-8030-92a2fda748ca'),(2,'Meelis','Reha','pass','user','ValiITTT','testimiseemail@gmail.com','edf78a80-70f9-47ae-9287-adee0c65ab4d'),(22,'Marek','Lints','pass','admin','BOSS','marek@hot.ee','3ffcc48b-972f-4b33-a542-099d784ef263'),(39,'Testime','testime','pass','user','ValiITTT','auto@hot.ee','29c9eff8-d175-4652-9ed4-3f00df6b84a5'),(40,'a','b','pass','user','Discovery','ant@ant.ees','d0744303-dfa0-405a-8577-f230a34629e0'),(41,'a','b','c','admin','Delivery','ant@ants.ees','6c216785-ab1f-4f53-bf04-5b5a73cb7d9d');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meeting`
--

DROP TABLE IF EXISTS `user_meeting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meeting`
--

LOCK TABLES `user_meeting` WRITE;
/*!40000 ALTER TABLE `user_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_meeting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `site_code` varchar(45) NOT NULL,
  `count` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mydb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18  9:45:03
